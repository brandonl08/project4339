class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :model
      t.string :color
      t.string :vin
      t.string :status

      t.timestamps null: false
    end
  end
end
