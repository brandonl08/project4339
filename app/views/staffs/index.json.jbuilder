json.array!(@staffs) do |staff|
  json.extract! staff, :id, :firstName, :lastName, :position
  json.url staff_url(staff, format: :json)
end
