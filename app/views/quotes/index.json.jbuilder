json.array!(@quotes) do |quote|
  json.extract! quote, :id, :firstname, :lastname, :number, :date, :price, :vin, :status
  json.url quote_url(quote, format: :json)
end
